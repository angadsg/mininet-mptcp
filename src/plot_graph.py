'''
Plots throughput on Y axis vs receive window size on X axis.

@author: garymm
@author: angad
'''
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot
import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument('--files', '-f',
                    help='CSV files from which to get values.',
                    required=False,
                    action='store',
                    nargs='+',
                    dest='files')
parser.add_argument('--out', '-o', help='Output PNG file will be written here.',
                    required=False,
                    dest='out')

args = parser.parse_args()

def parse_file(file_name):
    f = open(file_name, 'r')
    x_vals = []
    y_vals = []
    for line in f.xreadlines():
        x_val, y_val = line.split(',', 1)
        x_vals.append(x_val)
        y_vals.append(y_val)
    return x_vals, y_vals

def graph_files(files, output):
    pyplot.figure(figsize=(12,8), dpi=120)
    ax = pyplot.subplot(111)
    line_styles = ('-', '--')
    for i in range(len(files)):
        f = files[i]
        x_vals, y_vals = parse_file(f)
        # Hacks to make the legend say what we want.
        label = os.path.basename(
            f).replace('.csv', '').replace('_', ' ').replace(
            ' G', ' 3G').replace(' WiFi3G', '')
        line_style = line_styles[i % len(line_styles)]
        ax.plot( x_vals, y_vals, label=label, linestyle=line_style, linewidth=4)
    pyplot.ylabel('Throughput (Mbps)')
    pyplot.xlabel('Receive Window (KB)')
    pyplot.grid()
    # Hard-code y-axis limits to make the graphs consistent.
    pyplot.ylim([0,11])

    # Shink and shift current axis to make room for the legend.
    box = ax.get_position()
    ax.set_position([box.x0 - (box.width * 0.05), box.y0,
                     box.width * 0.75, box.height])
    # Set the legend off on the right side.
    ax.legend(loc='center right', bbox_to_anchor=(1.55, 0.5),
              fancybox=True, shadow=True)
    pyplot.savefig(output)


def main():
    graph_files(args.files, args.out)


if __name__ == '__main__':
    main()
