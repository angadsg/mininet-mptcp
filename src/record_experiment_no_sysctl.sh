#!/bin/bash

# Must be run as root.

# Exit on any failure
set -e

# Check for uninitialized variables
set -o nounset

ctrlc() {
	killall -9 python
	mn -c
	exit
}

trap ctrlc SIGINT


EXPERIMENT_ID=$(date +%b%d-%H:%M)

run_experiment() {
  python test_throughput.py --outputdir $EXPERIMENT_ID --prefix $1
  python test_throughput.py --outputdir $EXPERIMENT_ID --prefix "$1_jitter" \
    --add_jitter
}


# Run with regular TCP (MPTCP disabled):
sysctl -w net.mptcp.mptcp_enabled=0
run_experiment TCP

# Run with default MPTCP enabled:
sysctl -w net.mptcp.mptcp_enabled=1
run_experiment MPTCP-default

# Run with all optimizations disabled:
#sysctl -w net.mptcp.mptcp_rbuf_opti=0
#run_experiment MPTCP-no-optimizations

# Run with opportunistic retransmission enabled (Mechanism 1)
#sysctl -w net.mptcp.mptcp_rbuf_opti=1
#sysctl -w net.mptcp.mptcp_rbuf_retr=1
#sysctl -w net.mptcp.mptcp_rbuf_penal=0
#run_experiment MPTCP-M1

# Run with slow sub-flow penalisation (Mechanism 1+2)
#sysctl -w net.mptcp.mptcp_rbuf_penal=1
#run_experiment MPTCP-M1+M2

# Figure a: TCP and regular MPTCP
python plot_graph.py -f $EXPERIMENT_ID/{TCP_{WiFi,G},MPTCP-default_WiFi3G}.csv \
  -o $EXPERIMENT_ID/graph-a
python plot_graph.py -f $EXPERIMENT_ID/{TCP,MPTCP-default}_jitter*.csv \
  -o $EXPERIMENT_ID/graph-a-jitter
#python plot_graph.py -f $EXPERIMENT_ID/{TCP,MPTCP-no-optimizations}*.csv \
#  -o $EXPERIMENT_ID/graph-a
#python plot_graph.py \
#  -f $EXPERIMENT_ID/{TCP,MPTCP-no-optimizations}_jitter*.csv \
#  -o $EXPERIMENT_ID/graph-a-jitter
#
#
## Figure b: TCP WiFi and MPTCP+M1
#python plot_graph.py -f $EXPERIMENT_ID/{TCP_WiFi,MPTCP-M1_WiFi3G}*.csv \
#  -o $EXPERIMENT_ID/graph-b
#python plot_graph.py \
#  -f $EXPERIMENT_ID/{TCP_jitter_WiFi,MPTCP-M1_jitter_WiFi3G}*.csv \
#  -o $EXPERIMENT_ID/graph-b-jitter
#
#
## Figure c: TCP WiFi and MPTCP+M1,2
#python plot_graph.py -f $EXPERIMENT_ID/{TCP_WiFi,MPTCP-M1+M2_WiFi3G}*.csv \
#  -o $EXPERIMENT_ID/graph-c
#python plot_graph.py \
#  -f $EXPERIMENT_ID/{TCP_jitter_WiFi,MPTCP-M1+M2_jitter_WiFi3G}*.csv \
#  -o $EXPERIMENT_ID/graph-c-jitter
#
## New figure (d): MPTCP+M1,2 vs MPTCP trunk with default settings.
#python plot_graph.py -f $EXPERIMENT_ID/MPTCP-{M1+M2,default}_WiFi3G.csv \
#  -o $EXPERIMENT_ID/graph-d
#python plot_graph.py -f $EXPERIMENT_ID/MPTCP-{M1+M2,default}_jitter_WiFi3G.csv \
#  -o $EXPERIMENT_ID/graph-d-jitter