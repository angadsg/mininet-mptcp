from mininet.topo import Topo
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.net import Mininet
from mininet.util import dumpNodeConnections
import argparse
import datetime
import os
import time
import topology

parser = argparse.ArgumentParser()
timestamp = datetime.datetime.now().strftime("%d-%m-%y-%H-%M-%S")
parser.add_argument('--outputdir', '-o',
                    help='Output directory for storing the results',
                    required=False,
                    default='results-%s' % timestamp,
                    action='store',
                    dest='outputdir')
parser.add_argument('--prefix', '-p',
                    help='File name prefix',
                    required=False,
                    default='',
                    action='store',
                    dest='prefix')
parser.add_argument('--jitter', '-j',
                    help='How much jitter to add, as a fraction of link '
                         'latency. It will be doubled for the 3G link.',
                    required=False, default=0.0, action='store', type=float,
                    dest='jitter')
parser.add_argument('--num_trials', '-n',
                    help='How many times to repeat each test. Will output '
                         'median value.',
                    required=False, default=1,
                    action='store', type=int, dest='num_trials')
parser.add_argument('--half_queues', '-hq',
                    help='Halve the queue lengths.',
                    required=False, default=False,
                    action='store_true', dest='half_queues')

args = parser.parse_args()




def setUpRouting(net):
    """Sets up the routing tables so all interfaces can be used."""
    client = net.getNodeByName('client')
    server = net.getNodeByName('server')
    h = 1
    for host in (client, server):
        for i, interface in host.intfs.iteritems():
            mask_bits = 24
            addr = '192.168.%d.%d' % (i, h)
            interface.setIP(addr, prefixLen=mask_bits)
            host.cmd('ip rule add from %s/%d table %d' %
                     (interface.IP(), mask_bits, i))
            host.cmd('ip route add 192.168.%d.0/%d dev %s scope link table %d' %
                     (i, mask_bits, interface.name, i))
        h += 1


def testIperf(net):
    """Tests the bandwidth between client and server."""
    client = net.getNodeByName('client')
    server = net.getNodeByName('server')
    timestamp = str(datetime.datetime.now()).split()[1]
    server.cmd('iperf -s > %s/%s-server-iperf-%s.txt &' % (
               args.outputdir, args.prefix, timestamp))
    iperf_secs = 37
    print 'Testing bandwidth. Please wait %d seconds.' % iperf_secs
    client.cmd('iperf -c %s -t %d > %s/%s-client-iperf-%s.txt &' %
               (server.intf().IP(), iperf_secs, args.outputdir, args.prefix,
                timestamp))
    megabits_per_sec = measureThroughput(net, iperf_secs, timestamp)
    client.cmd('killall -9 iperf')
    time.sleep(2)
    return megabits_per_sec


def measureThroughput(net, iperf_secs, timestamp):
    client = net.getNodeByName('client')
    intfs = ','.join([str(val) for val in client.intfs.values()])
    # We let the connection ramp up for 5 seconds, and we ignore the last 2
    # seconds.
    warmup_secs = 5
    cooldown_secs = 2
    samples_per_sec = 2
    assert iperf_secs > warmup_secs + cooldown_secs
    bwm_secs = iperf_secs - (warmup_secs + cooldown_secs)
    bwm_command = (
        'bwm-ng --timeout %d --count %d --dynamic 0 --type sum --output csv '
        '--interfaces %s' %
        (1000 / samples_per_sec, (bwm_secs) * samples_per_sec, intfs))
    client.cmd('sleep %d' % warmup_secs)
    bwm_out = client.cmd(bwm_command)
    # bwm-ng format:
    # unix timestamp;iface_name;bytes_out;bytes_in;bytes_total;
    # packets_out;packets_in;packets_total;errors_out;errors_in\n
    log_file_name = os.path.join(args.outputdir,
                                 '%s-bwm-ng-%s.txt' % (args.prefix, timestamp))
    open(log_file_name, 'w').write(bwm_out)
    bytes_out = float(bwm_out.split()[-1].split(';')[2])
    
    megabits_per_sec = (bytes_out * 8.00 / 1000000) / bwm_secs
    return megabits_per_sec

def setRmem(net, rmem):
    """Sets the TCP receive window size."""
    server = net.getNodeByName('server')
    print 'Setting TCP Receive window to %dKB' % (rmem / 1024)
    server.cmd('sysctl -w net.ipv4.tcp_rmem=\'%d %d %d\'' % (rmem, rmem, rmem))


def median(vals):
    """Return median from an unsorted list of values.
    
    Copied from CS244 programming assignment 2 starter code.
    """
    s = sorted(vals)
    # Odd number of values: return the middle one.
    if len(s) % 2 == 1:
        return s[(len(vals) + 1) / 2 - 1]
    # Even number of values: return the mean of the two middle values.
    else:
        lower = s[len(vals) / 2 - 1]
        upper = s[len(vals) / 2]
        return float(lower + upper) / 2


def testThroughput(net, outFile, num_trials):
    """Varies the receive window (rmem) over a range of values
    and tests the bandwidth between the client and the server.
    Writes the receive window and bandwidth to outFile """
    kb = 1024
    for rmem in [10*kb] + range(200*kb, 1200*kb, 100*kb):
        setRmem(net, rmem)
        results = []
        for _ in range(num_trials):
            results.append(testIperf(net))
        out = "%d,%f\n" % (rmem/kb, median(results))
        print out,
        outFile.write(out)

def testTopo(topo, num_trials):
    topoName = type(topo).__name__.rstrip('Topo')
    print '-----------------------------------------------'
    print 'Testing throughput for %s.' % topoName
    net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink,
                  cleanup=True, autoPinCpus=True)
    net.start()
    net.pingAll()

    if not os.path.exists(args.outputdir):
        os.makedirs(args.outputdir)
 
    log_name = '%s/%s_%s.csv' % (args.outputdir, args.prefix, topoName)
    log = open(log_name, 'w')
    setUpRouting(net)
    testThroughput(net, log, num_trials)
  
    log.flush()
    log.close()
    net.stop()

def main():
    # We are only interested in the dual-path topology for MPTCP.
    if args.prefix.startswith('MPTCP'):
        topos = (topology.WiFi3GTopo(args.jitter, args.half_queues),)
    # And both of the single path topologies for TCP.
    else:
        topos = (topology.WiFiTopo(args.jitter, args.half_queues),
                 topology.GTopo(args.jitter, args.half_queues))
    for topo in topos:
        testTopo(topo, args.num_trials)
    

if __name__ == '__main__':
    main()
