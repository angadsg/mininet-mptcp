
client ip address del 10.0.0.1/8 brd + dev client-eth0
server ip address del 10.0.0.2/8 brd + dev server-eth0

client ip address add 192.168.1.2/24 brd + dev client-eth0
client ip address add 192.168.2.2/24 brd + dev client-eth1

server ip address add 192.168.1.3/24 brd + dev server-eth0
server ip address add 192.168.2.3/24 brd + dev server-eth1

client ip rule add from 192.168.1.2 table 1
client ip rule add from 192.168.2.2 table 2

server ip rule add from 192.168.1.3 table 1
server ip rule add from 192.168.2.3 table 2

client ip route add 192.168.1.0/24 dev client-eth0 scope link table 1
client ip route add 192.168.2.0/24 dev client-eth1 scope link table 2

server ip route add 192.168.1.0/24 dev server-eth0 scope link table 1
server ip route add 192.168.2.0/24 dev server-eth1 scope link table 2