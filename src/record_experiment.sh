#!/bin/bash

# Must be run as root.

# Exit on any failure
set -e

# Check for uninitialized variables
set -o nounset

ctrlc() {
	killall -9 python
	mn -c
	exit
}

trap ctrlc SIGINT


EXPERIMENT_ID=$(date +%b%d-%H:%M)

run_experiment() {
  python test_throughput.py --outputdir $EXPERIMENT_ID \
    --prefix $1 --jitter $2 --use_tbf 1
}

run_experiment_half_queues() {
  python test_throughput.py --outputdir $EXPERIMENT_ID \
    --prefix $1-half-queues --jitter 0 --half_queues	
}

run_experiment_htb() {
  python test_throughput.py --outputdir $EXPERIMENT_ID \
    --prefix $1 --jitter $2
}

LOW_JITTER="0.05"
HIGH_JITTER="0.25"

# HTB

# Run with regular TCP (MPTCP disabled):
sysctl -w net.mptcp.mptcp_enabled=0
run_experiment TCP 0
run_experiment_htb TCP_htb 0
# run_experiment_half_queues TCP 0
run_experiment TCP_low_jitter $LOW_JITTER
run_experiment TCP_high_jitter $HIGH_JITTER

# Run with MPTCP, all optimizations disabled:
sysctl -w net.mptcp.mptcp_enabled=1
sysctl -w net.mptcp.mptcp_rbuf_opti=0
sysctl -w net.mptcp.mptcp_rbuf_retr=0
sysctl -w net.mptcp.mptcp_rbuf_penal=0
run_experiment MPTCP-no-optimizations 0
run_experiment_htb MPTCP-no-optimizations_htb 0

# Half queues
# run_experiment_half_queues MPTCP-no-optimizations 0

# Run with opportunistic retransmission enabled (Mechanism 1)
sysctl -w net.mptcp.mptcp_rbuf_opti=1
sysctl -w net.mptcp.mptcp_rbuf_retr=1
sysctl -w net.mptcp.mptcp_rbuf_penal=0
run_experiment MPTCP-M1 0

# Run with Mechanism 1, half queue sizes
# run_experiment_half_queues MPTCP-M1 0

# Run with slow sub-flow penalisation (Mechanism 1+2)
sysctl -w net.mptcp.mptcp_rbuf_penal=1
run_experiment MPTCP-M1+M2 0

# With jitter
run_experiment MPTCP-M1+M2_low_jitter $LOW_JITTER
run_experiment MPTCP-M1+M2_high_jitter $HIGH_JITTER

# Figure a: TCP and regular MPTCP
python plot_graph.py \
  -f $EXPERIMENT_ID/{TCP_{WiFi,G},MPTCP-no-optimizations_WiFi3G}.csv \
  -o $EXPERIMENT_ID/graph-a

python plot_graph.py \
  -f $EXPERIMENT_ID/{TCP,MPTCP-no-optimizations}_htb*.csv \
  -o $EXPERIMENT_ID/graph-a-htb

# Figure b: TCP WiFi and MPTCP+M1
python plot_graph.py -f $EXPERIMENT_ID/{TCP_WiFi,MPTCP-M1_WiFi3G}.csv \
  -o $EXPERIMENT_ID/graph-b

# Figure c: TCP WiFi and MPTCP+M1,2
python plot_graph.py -f $EXPERIMENT_ID/{TCP_WiFi,MPTCP-M1+M2_WiFi3G}.csv \
  -o $EXPERIMENT_ID/graph-c

# Figure d: Low jitter
python plot_graph.py -f $EXPERIMENT_ID/{TCP,MPTCP-M1+M2}_low_jitter*.csv \
  -o $EXPERIMENT_ID/graph-d

# Figure e: High jitter
python plot_graph.py -f $EXPERIMENT_ID/{TCP,MPTCP-M1+M2}_high_jitter*.csv \
  -o $EXPERIMENT_ID/graph-e
