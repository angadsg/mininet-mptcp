from mininet.topo import Topo

class MyTopo( Topo ):
    "Simple topology example."

    def __init__( self, enable_all = True ):
        "Create custom topo."

        # Add default members to class.
        super( MyTopo, self ).__init__()

        # Set Node IDs for hosts and switches
#        client = 1;
#        switch_wifi = 2;
#        switch_3g = 3;
#        server = 4;

        # Add nodes
#        self.add_node( client, Node( is_switch=False ) )
#        self.add_node( switch_wifi, Node( is_switch=True ) )
#        self.add_node( switch_3g, Node( is_switch=True ) )
#        self.add_node( server, Node( is_switch=False ) )
#
#        # Add edges
#        self.add_edge( client, switch_wifi )
#        self.add_edge( client, switch_3g )
#        self.add_edge( server, switch_wifi )
#        self.add_edge( server, switch_3g )

        config = {'bw':10 }
        
        wifi_port = 0;
        client_port = 0;
        server_port = 1;
                
        server = self.add_host('server')
        client = self.add_host('client')
        switch_wifi = self.add_switch('s0')
        
        self.add_link(switch_wifi, client, port1=client_port, port2=wifi_port, **config);        
        self.add_link(switch_wifi, server, port1=server_port, port2=wifi_port, **config);
        
        
        # Consider all switches and hosts 'on'
#        self.enable_all()

topos = { 'mytopo': ( lambda: MyTopo() ) } 