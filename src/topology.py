from mininet.topo import Topo

def set_max_queue_size(link_config, buffer_ms):
    # bw Mb/s * s/ms * buffer_ms * b/Mb * packets / bit = # packets 
    buffer_packets = (
        link_config['bw'] / 1000.0 * buffer_ms * 1000000.0 / (1500.0 * 8.0))
    link_config['max_queue_size'] = buffer_packets

class BaseTopo(Topo):
    def __init__(self, jitter, half_queues=False):
        "Populates configurations for WiFi and 3G links and ports."

        super(BaseTopo, self).__init__()
        # delay = RTT / 2
        wifi_delay = 10.0
        wifi_delay_string = '%fms' % wifi_delay
        if jitter:
            jitter_ms = jitter * wifi_delay
            wifi_delay_string += ' %fms distribution pareto' % jitter_ms
        self.wifi_config = {'bw': 8, 'delay': wifi_delay_string}
        buffer_ms = 80
        if half_queues:
            buffer_ms *= 0.5
        set_max_queue_size(self.wifi_config, buffer_ms)
        g_delay = 75.0
        g_delay_string = '%fms' % g_delay
        if jitter:
            # Twice as much jitter on the 3G link.
            jitter_ms = 2.0 * jitter * g_delay
            g_delay_string += ' %fms distribution pareto' % jitter_ms
        self.g_config = {'bw': 2, 'delay': g_delay_string}
        buffer_ms = 2*1000
        if half_queues:
            buffer_ms *= 0.5
        set_max_queue_size(self.g_config, buffer_ms)

        # Server's links are really fast.
        self.server_config = {'bw': 100, 'delay': '0.1ms'}

        self.wifi_port = 0;
        self.g_port = 1;
        self.client_port = 0;
        self.server_port = 1;
        

class WiFi3GTopo(BaseTopo):
    """
    Two links from the client:
      1. 8Mbps WiFi-like path (base RTT 20ms, 80ms buffer)
      2. 2Mbps 3G path (base RTT 150ms, 2s buffer).
    """

    def __init__(self, jitter, half_queues=False):
        # Add default members to class.
        super(WiFi3GTopo, self).__init__(jitter, half_queues)
        
        server = self.add_host('server')
        client = self.add_host('client')
        switch_wifi = self.add_switch('s0')
        switch_3g = self.add_switch('s1')

        self.add_link(
            switch_wifi, client, port1=self.client_port, port2=self.wifi_port,
            **self.wifi_config)
        self.add_link(
            switch_3g, client, port1=self.client_port, port2=self.g_port, 
            **self.g_config)

        self.add_link(
            switch_wifi, server, port1=self.server_port, port2=self.wifi_port,
            **self.server_config)
        self.add_link(
            switch_3g, server, port1=self.server_port, port2=self.g_port,
            **self.server_config)
    
class WiFiTopo(BaseTopo):
    """
    One link from the client:
        8Mbps WiFi-like path (base RTT 20ms, 80ms buffer)
    """

    def __init__(self, jitter, half_queues=False):

        # Add default members to class.
        super(WiFiTopo, self).__init__(jitter, half_queues)
        
        server = self.add_host('server')
        client = self.add_host('client')
        switch_wifi = self.add_switch('s0')

        self.add_link(
            switch_wifi, client, port1=self.client_port, port2=self.wifi_port,
            **self.wifi_config)

        self.add_link(
            switch_wifi, server, port1=self.server_port, port2=self.wifi_port,
            **self.server_config)
    
    
class GTopo(BaseTopo):
    """
    One link from the client:
       2Mbps 3G path (base RTT 150ms, 2s buffer).
    """

    def __init__(self, jitter, half_queues=False):
        "Create custom topo."

        # Add default members to class.
        super(GTopo, self).__init__(jitter, half_queues)
        
        server = self.add_host('server')
        client = self.add_host('client')
        switch_3g = self.add_switch('s1')


        self.add_link(
            switch_3g, client, port1=self.client_port, port2=self.g_port, 
            **self.g_config)

        self.add_link(
            switch_3g, server, port1=self.server_port, port2=self.g_port,
            **self.server_config)
    

topos = {'wifi3g': (lambda: WiFi3GTopo(0)), 'wifi': (lambda: WiFiTopo(0)),
         '3g': (lambda: GTopo(0))}
